package com.love2learn.controller;

import com.love2learn.model.User;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class UserController {
    @GetMapping("/users")
    public ResponseEntity<User> getUserDetails(@RequestParam String firstName, @RequestParam String lastName){
        User user = new User();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        return new ResponseEntity<>( user, HttpStatus.OK);
    }
}
